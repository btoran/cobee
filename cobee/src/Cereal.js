import React, { Component } from 'react';
import './css/Cereal.css';
import wuyi from './Assets/Wuyi.png';
import ceylon from './Assets/Ceylon.png';
import lapsang from './Assets/Lapsang.png';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


export default class Cereal extends Component {

    constructor(props){
        super(props);
        this.state = {
            indexCereal: 0,
            urlName: wuyi
        }
    }

   componentDidUpdate(prevProps, prevState, snapshot) {

        if ( this.props.indexCereal !== this.state.indexCereal){

            let url;

            switch (this.props.cereals[this.props.indexCereal].color) {

                case 'yellow':
                    url= wuyi;
                    break;
                case 'white':
                    url = lapsang;
                    break;
                case 'brown':
                    url=ceylon;
                    break;
                default:
                   break;

            }

            this.setState({indexCereal:this.props.indexCereal, urlName:url});
        }

   }

    render() {

        const cereals =   <img key={this.state.urlName} className={'cereal'} src={this.state.urlName} alt={'cereal'}/>;

        return (

            <ReactCSSTransitionGroup
                transitionName="animationCereal"
                transitionEnterTimeout={1000}
                transitionLeaveTimeout={1000}>
                {cereals}
            </ReactCSSTransitionGroup>

        )
    }
}

