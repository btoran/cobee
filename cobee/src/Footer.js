import React, { Component } from 'react';
import './css/Footer.css'
import {Button, Col, Container, Row} from "reactstrap";
import * as cereals from "./Assets/cereals";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

export default class Footer extends Component {


    constructor(props){
        super(props);
        this.state = {
            indexCereal: 0,
            priceCereal:''
        }
    }

    componentDidMount() {

        let firstCereal = this.props.cereals[0];
        this.setState({priceCereal:firstCereal.price});
    }

    changeIndexCereal = () => {

        let index = this.state.indexCereal;
        index++;
        let price=0;

        if (index > 2){
            index = 0;
        }

        this.props.cereals.forEach((cereal,i) => {
            if (i ===index){
               price = cereal.price;
            }
        });

        this.setState({indexCereal:index, priceCereal:price});
        this.props.changeIndexCereal(index);

    };

    render() {

        const price = <div key={this.state.priceCereal} className={'price'}>{this.state.priceCereal}</div>;

        return (
                <div className={'footerRight'}>
                    <Container>
                        <Row>
                            <Col sm={8}>
                                <ReactCSSTransitionGroup
                                    transitionName="animationPrice"
                                    transitionEnterTimeout={1000}
                                    transitionLeaveTimeout={1000}>
                                    {price}
                                </ReactCSSTransitionGroup>
                                {/*<span className={'price'}>{this.state.priceCereal}</span>*/}
                            </Col>
                            <Col sm={4}>
                                <Row>
                                    <Col xs={3}>
                                        <div className={'numberProducts'} >{this.state.indexCereal}</div>
                                    </Col>
                                    <Col xs={9}>
                                        <Button  color="primary" className={'addCartButton'} onClick={this.changeIndexCereal}>ADD TO CART</Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </div>
        );
    }
}
