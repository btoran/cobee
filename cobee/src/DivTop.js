import React, { Component } from 'react';
import './css/DivTop.css'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {Input} from "reactstrap";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

library.add(faBars);

export default class DivTop extends Component {

    constructor(props){
        super(props);
        this.state = {
            indexCereal: 0,
            nameCereals: ''
        }
    }

    componentDidMount() {

        let firstCereal = this.props.cereals[0];
        let name = firstCereal.name.substr(0,this.props.cereals[this.props.indexCereal].name.indexOf(' '));
        name = name + 'Top';
        this.setState({priceCereal:name});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        let name = this.props.cereals[this.props.indexCereal].name
                  .substr(0,this.props.cereals[this.props.indexCereal].name.indexOf(' '));

        name = name + 'Top';

        if ( this.props.indexCereal !== this.state.indexCereal){
             this.setState({indexCereal:this.props.indexCereal, nameCereals:[name]});
        }
    }

    render() {
        const divs = <div key={this.state.nameCereals} className={'backgroundTop ' + this.state.nameCereals}/>;

        return(
            <div className={this.state.nameCereals + ' navBar'}>

                <div className={'boxNavBar categories'}>
                    <FontAwesomeIcon icon='bars'/>
                    <span className={'categories'}>categories</span>
                </div>
                <div className={'boxNavBar'}>about
                </div>
                <div className={'boxNavBar'}>contact
                </div>
                <div className={'boxNavBar'}>
                    <Input type="text" name="search" className="search" placeholder="Search"/>
                </div>

            <ReactCSSTransitionGroup
                transitionName="animationDivTop"
                transitionEnterTimeout={1000}
                transitionLeaveTimeout={1000}>
                {divs}
            </ReactCSSTransitionGroup>
        </div>
        )
    }
}
