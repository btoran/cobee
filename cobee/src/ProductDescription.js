import React, { Component } from 'react';
import './css/ProductDescription.css'
import {Col, Container, Row} from "reactstrap";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";



export default class ProductDescription extends Component {

    constructor(props){
        super(props);
        this.state = {
            indexCereal: 0,
            typeCereal: '',
            nameCereal: '',
            descriptionCereal:'',
            heightCereal:'',
            mouthfeelCereal:'',
        }
    }

    componentDidMount() {

        let firstCereal = this.props.cereals[0];
        this.setState({nameCereal:firstCereal.name,
                             typeCereal: firstCereal.type,
                             descriptionCereal:firstCereal.description,
                             mouthfeelCereal: firstCereal.mouthfeel,
                             heightCereal:firstCereal.height});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        let name = this.props.cereals[this.props.indexCereal].name;
        let type = this.props.cereals[this.props.indexCereal].type;
        let description = this.props.cereals[this.props.indexCereal].description;
        let mouthfeel = this.props.cereals[this.props.indexCereal].mouthfeel;
        let height = this.props.cereals[this.props.indexCereal].height;


        if ( this.props.indexCereal !== this.state.indexCereal){
            this.setState({indexCereal:this.props.indexCereal,
                                 nameCereal: name,
                                 typeCereal:type,
                                 descriptionCereal: description,
                                 mouthfeelCereal:mouthfeel,
                                 heightCereal:height});
        }
    }

    render() {

        const typeCereal =  <div key={this.state.typeCereal}>{this.state.typeCereal}</div>;
        const nameCereal =  <div key={this.state.nameCereal}>{this.state.nameCereal}</div>;
        const descriptionCereal =  <div key={this.state.descriptionCereal}>{this.state.descriptionCereal}</div>;
        const mouthfeelCereal =  <div key={this.state.mouthfeelCereal}>{this.state.mouthfeelCereal}</div>;
        const heightCereal =  <div key={this.state.heightCereal}>{this.state.heightCereal}</div>;

        return (
            <div>
                <div className={'productDescriptionSide'}>
                    <div className={'productText'}>
                        {this.props.cereals.map((cereal,i) => {
                                if (i===this.props.indexCereal) {
                                    return (
                                        <Container>
                                            <Row>
                                                <Col  md={{ size: 3, offset: 5 }} className={['boxType','box']}>
                                                    <ReactCSSTransitionGroup
                                                        transitionName="animationTypeCereal"
                                                        transitionEnterTimeout={1000}
                                                        transitionLeaveTimeout={1000}>
                                                        {typeCereal}
                                                    </ReactCSSTransitionGroup>
                                                    )
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col  md={{ size: 6, offset: 2 }} className={['boxName','box']}>
                                                    <ReactCSSTransitionGroup
                                                        transitionName="animationNameCereal"
                                                        transitionEnterTimeout={1000}
                                                        transitionLeaveTimeout={1000}>
                                                        {nameCereal}
                                                    </ReactCSSTransitionGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={{ size: '4', offset: 1 }} className={['boxDescription','box']}>
                                                    <ReactCSSTransitionGroup
                                                        transitionName="animationDescriptionCereal"
                                                        transitionEnterTimeout={1000}
                                                        transitionLeaveTimeout={1000}>
                                                        {descriptionCereal}
                                                    </ReactCSSTransitionGroup>
                                                </Col>
                                                <Col md='6'>
                                                    <Row>
                                                        <Col md="4" className={['boxTaste','box']}>
                                                            <div className={'titleBox'}>Taste</div>
                                                            {cereal.taste.map((taste, i) => {
                                                                    return <div className={'subtitleBox'}>{taste}</div>
                                                                }
                                                            )}
                                                        </Col>
                                                        <Col md="4" className={['boxAroma','box']}>
                                                            <div className={'titleBox'}>Aroma</div>
                                                            {cereal.aroma.map((aroma, i) => {
                                                                    return <div className={'subtitleBox'}>{aroma}</div>
                                                                }
                                                            )}

                                                        </Col>
                                                        <Col md="4" className={['boxMouthfeel','box']}>
                                                            <div className={'titleBox'}>Mouthfeel</div>
                                                            <ReactCSSTransitionGroup
                                                                transitionName="animationMouthfeelCereal"
                                                                transitionEnterTimeout={1000}
                                                                transitionLeaveTimeout={1000}>
                                                                {mouthfeelCereal}
                                                            </ReactCSSTransitionGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md="3" className={['boxHeight','box']}>

                                                            <ReactCSSTransitionGroup
                                                                transitionName="animationTypeCereal"
                                                                transitionEnterTimeout={1000}
                                                                transitionLeaveTimeout={1000}>
                                                                {heightCereal}
                                                            </ReactCSSTransitionGroup>
                                                        </Col>

                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Container>
                                    )
                                }
                            }
                        )}
                    </div>
                </div>
            </div>

        );
    }
}
