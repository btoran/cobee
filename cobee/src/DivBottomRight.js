import React, { Component } from 'react';
import './css/DivBottomRight.css';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


export default class DivBottomRight extends Component {

    constructor(props){
        super(props);
        this.state = {
            indexCereal: 0,
            nameCereals: ['WUYIRight']
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        let name = this.props.cereals[this.props.indexCereal].name.substr(0,this.props.cereals[this.props.indexCereal].name.indexOf(' '));
        name = name + 'Right';
        console.log(name);

         if ( this.props.indexCereal !== this.state.indexCereal){
             this.setState({indexCereal:this.props.indexCereal, nameCereals:[name]});
         }

    }

    render() {
        const divs = <div key={this.state.nameCereals} className={'backgroundBottomRight ' + this.state.nameCereals}/>;

        return(
            <ReactCSSTransitionGroup
                transitionName="animationDivBottomRight"
                transitionEnterTimeout={1000}
                transitionLeaveTimeout={1000}>
                {divs}
            </ReactCSSTransitionGroup>
        )
    }
}
