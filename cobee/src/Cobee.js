import React, { Component } from 'react';
import './css/Cobee.css'
import Footer from "./Footer";
import ProductDescription from "./ProductDescription";
import * as cereals from "./Assets/cereals";
import Cereal from "./Cereal";
import DivBottomLeft from "./DivBottomLeft";
import DivBottomRight from "./DivBottomRight";
import DivTop from "./DivTop";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

library.add(faShoppingBag);

export default class Cobee extends Component {

    constructor(props){
        super(props);
        this.state = {
            cereals : cereals.results,
            indexCereal: 0
        }
    }
    changeIndexCereal = (index) => {

        this.setState({indexCereal:index});
    };

    render() {
        return (
            <div className={'background'}>

                <DivTop indexCereal={this.state.indexCereal} cereals = {this.state.cereals}/>
                <h2 className={'title'}>Bennet</h2>
                <div className={'bag'}> <FontAwesomeIcon icon='shopping-bag'/></div>
                <Cereal indexCereal={this.state.indexCereal} changeIndexCereal={this.changeIndexCereal} cereals = {this.state.cereals}/>
                <DivBottomLeft indexCereal={this.state.indexCereal} cereals = {this.state.cereals}/>
                <DivBottomRight indexCereal={this.state.indexCereal} cereals = {this.state.cereals}/>
                <ProductDescription indexCereal={this.state.indexCereal} cereals = {this.state.cereals}/>
                <Footer changeIndexCereal={this.changeIndexCereal} cereals = {this.state.cereals}/>


            </div>
        );
    }
}
